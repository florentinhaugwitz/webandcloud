"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.getAverageCategoryData = exports.getCountCategoryData = exports.getCountRatingData = exports.addOneBook = exports.getOneBook = exports.getBooksByCategory = exports.getAllBooks = void 0;
const init_1 = require("./init");
function getAllBooks(search, fn) {
    const sql = `
              SELECT b.*, json_group_array(a.name) AS authors
              FROM Book AS b
              INNER JOIN author_book AS ab ON b.id = ab.book_id
              INNER JOIN author AS a ON ab.author_id = a.id
              WHERE b.title LIKE '%' || ? || '%' OR a.name LIKE '%' || ? || '%'
              GROUP BY b.id
              `;
    const params = [search, search];
    return init_1.db.all(sql, params, (err, rows) => {
        if (err) {
            console.log("error in database: " + err);
            fn([]);
        }
        else {
            rows.forEach((element, index, array) => {
                array[index].authors = JSON.parse(element.authors);
            });
            fn(rows);
        }
    });
}
exports.getAllBooks = getAllBooks;
function getBooksByCategory(search, fn) {
    const sql = `
              SELECT b.*, json_group_array(a.name) AS authors
              FROM Book AS b
              INNER JOIN author_book AS ab ON b.id = ab.book_id
              INNER JOIN author AS a ON ab.author_id = a.id
              WHERE b.category = (?)
              GROUP BY b.id
              `;
    const params = [search];
    return init_1.db.all(sql, params, (err, rows) => {
        if (err) {
            console.log("error in database: " + err);
            fn([]);
        }
        else {
            rows.forEach((element, index, array) => {
                array[index].authors = JSON.parse(element.authors);
            });
            fn(rows);
        }
    });
}
exports.getBooksByCategory = getBooksByCategory;
function getOneBook(id, fn) {
    const sql = `
              SELECT b.*, json_group_array(a.name) AS authors
              FROM Book AS b
              INNER JOIN author_book AS ab ON b.id = ab.book_id
              INNER JOIN author AS a ON ab.author_id = a.id
              WHERE b.id = ?
              GROUP BY b.id
              `;
    const params = ["" + id];
    return init_1.db.get(sql, params, (err, row) => {
        if (err) {
            console.log("error in database: " + err);
            fn(null);
        }
        else {
            row.authors = JSON.parse(row.authors);
            fn(row);
        }
    });
}
exports.getOneBook = getOneBook;
function addOneBook(s) {
    init_1.db.serialize(() => {
        const insertBook = `
      INSERT INTO book (id,title, image, rating, numberrating, category) VALUES (?,?,?,?,?,?)
      `;
        init_1.db.run(insertBook, [s.id, s.title, s.image, s.rating, s.numberrating, s.category]);
        const insertAuthor = `
      INSERT OR IGNORE INTO author (name) VALUES (?)
      `;
        s.authors.forEach(author => {
            init_1.db.run(insertAuthor, [author]);
        });
        const insertAuthorBook = `
      INSERT INTO author_book (author_id, book_id) VALUES (?,?)
      `;
        const selAuthor = `
      SELECT id FROM author WHERE name = (?)
      `;
        const selBook = `
      SELECT MAX(id) AS max_id FROM book
      `;
        s.authors.forEach(author => {
            init_1.db.get(selAuthor, author, (errSelAuthor, authorId) => {
                if (errSelAuthor) {
                    console.log("Error in database: " + errSelAuthor);
                }
                else {
                    init_1.db.get(selBook, (errSelBook, bookId) => {
                        if (errSelBook) {
                            console.log("Error in database: " + errSelBook);
                        }
                        else {
                            init_1.db.run(insertAuthorBook, [authorId.id, bookId.max_id]);
                        }
                    });
                }
            });
        });
    });
}
exports.addOneBook = addOneBook;
function getCountRatingData(fn) {
    const sql = `
              SELECT rating, count(id) AS number_books
              FROM Book
              GROUP BY rating
              `;
    return init_1.db.all(sql, (err, rows) => {
        if (err) {
            console.log("error in database: " + err);
            fn([]);
        }
        else {
            fn(rows);
        }
    });
}
exports.getCountRatingData = getCountRatingData;
function getCountCategoryData(fn) {
    const sql = `
              SELECT category, count(id) AS number_books
              FROM Book
              GROUP BY category
              `;
    return init_1.db.all(sql, (err, rows) => {
        if (err) {
            console.log("error in database: " + err);
            fn([]);
        }
        else {
            fn(rows);
        }
    });
}
exports.getCountCategoryData = getCountCategoryData;
function getAverageCategoryData(fn) {
    const sql = `
              SELECT category, avg(rating) AS number_books
              FROM Book
              GROUP BY category
              `;
    return init_1.db.all(sql, (err, rows) => {
        if (err) {
            console.log("error in database: " + err);
            fn([]);
        }
        else {
            fn(rows);
        }
    });
}
exports.getAverageCategoryData = getAverageCategoryData;
//# sourceMappingURL=index.js.map