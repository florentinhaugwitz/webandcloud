"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.db = void 0;
const sqlite3_1 = __importDefault(require("sqlite3"));
const books_1 = require("../books");
const sqlite3 = sqlite3_1.default.verbose();
exports.db = new sqlite3.Database("db.sqlite", (err) => {
    if (err) {
        console.log(err.message);
        throw err;
    }
    else {
        console.log("Connected to the database");
        exports.db.serialize(() => {
            exports.db.run(`
                    CREATE TABLE author(
                        id INTEGER PRIMARY KEY AUTOINCREMENT,
                        name TEXT,
                        UNIQUE(name)
                    )
                    `, (dberrc) => {
                if (dberrc) {
                    console.log("Authors' table already created.");
                }
                else {
                    const insert = 'INSERT OR IGNORE INTO author (name) VALUES (?)';
                    books_1.books.forEach(b => {
                        b.authors.forEach(author => {
                            exports.db.run(insert, [author]);
                        });
                    });
                }
            });
            exports.db.run(`
                    CREATE TABLE book(
                        id INTEGER PRIMARY KEY,
                        title TEXT,
                        image TEXT,
                        rating INTEGER,
                        numberrating INTEGER,
                        category TEXT
                    )
                    `, (dberrc) => {
                if (dberrc) {
                    console.log("Books' table already created.");
                }
                else {
                    const insert = `
                            INSERT INTO book (id, title, image, rating, numberrating, category) VALUES (?,?,?,?,?,?)
                                `;
                    books_1.books.forEach(b => {
                        exports.db.run(insert, [b.id, b.title, b.image, b.rating, b.numberrating, b.category]);
                    });
                }
            });
            exports.db.run(`
                    CREATE TABLE author_book(
                        author_id INTEGER,
                        book_id INTEGER,
                        FOREIGN KEY(author_id) REFERENCES author(author_id),
                        FOREIGN KEY(book_id) REFERENCES book(book_id)
                    )
                    `, (dberrc) => {
                if (dberrc) {
                    console.log("Book/Author relation table already created.");
                }
                else {
                    const insert = `
                            INSERT INTO author_book (author_id, book_id) VALUES (?,?)
                                `;
                    const sel = `
                            SELECT id FROM author WHERE name = (?)
                                `;
                    books_1.books.forEach(b => {
                        b.authors.forEach(author => {
                            exports.db.get(sel, author, (errSel, authorId) => {
                                if (errSel) {
                                    console.log("Error in database: " + errSel);
                                }
                                else {
                                    exports.db.run(insert, [authorId.id, b.id]);
                                }
                            });
                        });
                    });
                }
            });
        });
    }
});
//# sourceMappingURL=init.js.map