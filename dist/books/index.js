"use strict";
// This module should contain all functions related with books
// For now, it only contains the list of books
Object.defineProperty(exports, "__esModule", { value: true });
exports.books = void 0;
exports.books = [
    {
        "isbn": "1588345297",
        "category": "Politics and Social Sciences",
        "title": "With Schwarzkopf",
        "authors": [
            "Gus Lee"
        ],
        "image": "https://covers.openlibrary.org/b/id/11333157-L.jpg",
        "rating": 5,
        "numberrating": 363,
        "id": 1
    },
    {
        "isbn": "1404803335",
        "category": "Children's Books",
        "title": "Magnets",
        "authors": [
            "Natalie M. Rosinsky"
        ],
        "image": "https://covers.openlibrary.org/b/id/4805314-L.jpg",
        "rating": 4,
        "numberrating": 227,
        "id": 2
    },
    {
        "isbn": "1405167971",
        "category": "Politics and Social Sciences",
        "title": "Running and Philosophy",
        "authors": [
            "Michael W. Austin"
        ],
        "image": "https://covers.openlibrary.org/b/id/2750337-L.jpg",
        "rating": 5,
        "numberrating": 367,
        "id": 3
    },
    {
        "isbn": "1430032308",
        "category": "Home and Living",
        "title": "Seamless",
        "authors": [
            "Angie Smith"
        ],
        "image": "https://covers.openlibrary.org/b/id/10865772-L.jpg",
        "rating": 2,
        "numberrating": 383,
        "id": 4
    },
    {
        "isbn": "1628090308",
        "category": "Home and Living",
        "title": "Unofficial guide to Universal Orlando",
        "authors": [
            "Seth Kubersky"
        ],
        "image": "https://covers.openlibrary.org/b/id/10819024-L.jpg",
        "rating": 5,
        "numberrating": 188,
        "id": 5
    },
    {
        "isbn": "1457532506",
        "category": "Literature and Fiction",
        "title": "Black Wall of Silence",
        "authors": [
            "Paul Morrissey"
        ],
        "image": "https://covers.openlibrary.org/b/id/12636481-L.jpg",
        "rating": 2,
        "numberrating": 312,
        "id": 6
    },
    {
        "isbn": "1618510177",
        "category": "Home and Living",
        "title": "Bah\u00e1'\u00ed Basics",
        "authors": [
            "Frances Worthington"
        ],
        "image": "https://covers.openlibrary.org/b/id/8612220-L.jpg",
        "rating": 5,
        "numberrating": 368,
        "id": 7
    },
    {
        "isbn": "1449430554",
        "category": "Home and Living",
        "title": "Salt block cooking",
        "authors": [
            "Mark Bitterman"
        ],
        "image": "https://covers.openlibrary.org/b/id/9482123-L.jpg",
        "rating": 1,
        "numberrating": 405,
        "id": 8
    },
    {
        "isbn": "189376219X",
        "category": "Home and Living",
        "title": "Twill thrills",
        "authors": [
            "Madelyn Van der Hoogt"
        ],
        "image": "https://covers.openlibrary.org/b/id/942956-L.jpg",
        "rating": 2,
        "numberrating": 193,
        "id": 9
    },
    {
        "isbn": "067944453X",
        "category": "Sports and Outdoors",
        "title": "Trout",
        "authors": [
            "James Prosek"
        ],
        "image": "https://covers.openlibrary.org/b/id/418499-L.jpg",
        "rating": 3,
        "numberrating": 373,
        "id": 10
    },
    {
        "isbn": "151224922X",
        "category": "Sports and Outdoors",
        "title": "Vegan recipes in 30 minutes",
        "authors": [
            "Amanda Rice"
        ],
        "image": "https://covers.openlibrary.org/b/id/11145103-L.jpg",
        "rating": 4,
        "numberrating": 56,
        "id": 11
    },
    {
        "isbn": "1905449437",
        "category": "Sports and Outdoors",
        "title": "Grovel!",
        "authors": [
            "David Tossell"
        ],
        "image": "https://covers.openlibrary.org/b/id/3049050-L.jpg",
        "rating": 3,
        "numberrating": 63,
        "id": 12
    },
    {
        "isbn": "1615305289",
        "category": "Teen and Young Adult",
        "title": "The Britannica guide to basketball",
        "authors": [
            "Adam Augustyn"
        ],
        "image": "https://covers.openlibrary.org/b/id/8625082-L.jpg",
        "rating": 1,
        "numberrating": 486,
        "id": 13
    },
    {
        "isbn": "1557788413",
        "category": "Education",
        "title": "The Fraternity",
        "authors": [
            "John Fitzgerald Molloy"
        ],
        "image": "https://covers.openlibrary.org/b/id/781141-L.jpg",
        "rating": 1,
        "numberrating": 487,
        "id": 14
    },
    {
        "isbn": "1567114407",
        "category": "Arts and Photography",
        "title": "Creating with mosaics",
        "authors": [
            "Anna Freixenet"
        ],
        "image": "https://covers.openlibrary.org/b/id/6805573-L.jpg",
        "rating": 2,
        "numberrating": 144,
        "id": 15
    },
    {
        "isbn": "155783847X",
        "category": "Humor and Entertainment",
        "title": "The Sound Of Music Family Scrapbook",
        "authors": [
            "Fred Bronson"
        ],
        "image": "https://covers.openlibrary.org/b/id/7731196-L.jpg",
        "rating": 4,
        "numberrating": 295,
        "id": 16
    },
    {
        "isbn": "1401310338",
        "category": "Humor and Entertainment",
        "title": "SoulPancake",
        "authors": [
            "Rainn Wilson"
        ],
        "image": "https://covers.openlibrary.org/b/id/10338597-L.jpg",
        "rating": 2,
        "numberrating": 93,
        "id": 17
    },
    {
        "isbn": "1402744315",
        "category": "Home and Living",
        "title": "Easy Needle Felting",
        "authors": [
            "Nancy Hoerner",
            "Judy Jacobs",
            "Kay Kaduce"
        ],
        "image": "https://covers.openlibrary.org/b/id/2740988-L.jpg",
        "rating": 1,
        "numberrating": 386,
        "id": 18
    },
    {
        "isbn": "1426206453",
        "category": "Arts and Photography",
        "title": "National Geographic",
        "authors": [
            "National Geographic Society (U.S.)"
        ],
        "image": "https://covers.openlibrary.org/b/id/12132332-L.jpg",
        "rating": 2,
        "numberrating": 391,
        "id": 19
    },
    {
        "isbn": "1578700779",
        "category": "Education",
        "title": "Developing IP multicast networks",
        "authors": [
            "Beau Williamson"
        ],
        "image": "https://covers.openlibrary.org/b/id/835969-L.jpg",
        "rating": 1,
        "numberrating": 265,
        "id": 20
    },
    {
        "isbn": "081382639X",
        "category": "Education",
        "title": "Dictionary of Veterinary Epidemiology",
        "authors": [
            "B. Toma"
        ],
        "image": "https://covers.openlibrary.org/b/id/2648444-L.jpg",
        "rating": 4,
        "numberrating": 211,
        "id": 21
    },
    {
        "isbn": "1780051034",
        "category": "Travel",
        "title": "Puerto Rico",
        "authors": [
            "Carine Tracanelli"
        ],
        "image": "https://covers.openlibrary.org/b/id/9253836-L.jpg",
        "rating": 1,
        "numberrating": 298,
        "id": 22
    },
    {
        "isbn": "1590843983",
        "category": "Teen and Young Adult",
        "title": "Ninjutsu (Martial and Fighting Arts)",
        "authors": [
            "Eric Chaline"
        ],
        "image": "https://covers.openlibrary.org/b/id/2951823-L.jpg",
        "rating": 1,
        "numberrating": 431,
        "id": 23
    },
    {
        "isbn": "155799756X",
        "category": "Home and Living",
        "title": "Daily Handwriting Practice",
        "authors": [
            "Jill Norris"
        ],
        "image": "https://covers.openlibrary.org/b/id/782051-L.jpg",
        "rating": 5,
        "numberrating": 87,
        "id": 24
    },
    {
        "isbn": "1558747540",
        "category": "Home and Living",
        "title": "From the First Bite",
        "authors": [
            "Kay Sheppard"
        ],
        "image": "https://covers.openlibrary.org/b/id/785372-L.jpg",
        "rating": 2,
        "numberrating": 307,
        "id": 25
    },
    {
        "isbn": "1556522096",
        "category": "Travel",
        "title": "Chicago on foot",
        "authors": [
            "Ira J. Bach"
        ],
        "image": "https://covers.openlibrary.org/b/id/777679-L.jpg",
        "rating": 4,
        "numberrating": 37,
        "id": 26
    },
    {
        "isbn": "1780922191",
        "category": "Sports and Outdoors",
        "title": "Tok258 Morgan Winner At Le Mans",
        "authors": [
            "Ronnie Price"
        ],
        "image": "https://covers.openlibrary.org/b/id/7705980-L.jpg",
        "rating": 4,
        "numberrating": 238,
        "id": 27
    },
    {
        "isbn": "1565238427",
        "category": "Home and Living",
        "title": "Complete Starter Guide to Whittling",
        "authors": [
            "Woodcarving Illustrated Staff"
        ],
        "image": "https://covers.openlibrary.org/b/id/12009903-L.jpg",
        "rating": 5,
        "numberrating": 260,
        "id": 28
    },
    {
        "isbn": "1608682048",
        "category": "Home and Living",
        "title": "The Joyous Cosmology",
        "authors": [
            "Alan Watts"
        ],
        "image": "https://covers.openlibrary.org/b/id/9026029-L.jpg",
        "rating": 5,
        "numberrating": 449,
        "id": 29
    },
    {
        "isbn": "1567921655",
        "category": "Teen and Young Adult",
        "title": "The Field and Forest Handy Book",
        "authors": [
            "Daniel Carter Beard",
            "David R. Godine"
        ],
        "image": "https://covers.openlibrary.org/b/id/810174-L.jpg",
        "rating": 3,
        "numberrating": 422,
        "id": 30
    },
    {
        "isbn": "159116589X",
        "category": "Teen and Young Adult",
        "title": "Case Closed, Vol. 3",
        "authors": [
            "Gosho Aoyama"
        ],
        "image": "https://covers.openlibrary.org/b/id/863436-L.jpg",
        "rating": 2,
        "numberrating": 138,
        "id": 31
    },
    {
        "isbn": "1594745692",
        "category": "Humor and Entertainment",
        "title": "The Zombie Tarot Cards",
        "authors": [
            "Paul Kepple"
        ],
        "image": "https://covers.openlibrary.org/b/id/7834152-L.jpg",
        "rating": 4,
        "numberrating": 132,
        "id": 32
    },
    {
        "isbn": "1590300068",
        "category": "Teen and Young Adult",
        "title": "The  wave in the mind",
        "authors": [
            "Ursula K. Le Guin"
        ],
        "image": "https://covers.openlibrary.org/b/id/859441-L.jpg",
        "rating": 4,
        "numberrating": 221,
        "id": 33
    },
    {
        "isbn": "1591165873",
        "category": "Teen and Young Adult",
        "title": "Case Closed, Vol. 2",
        "authors": [
            "Gosho Aoyama"
        ],
        "image": "https://covers.openlibrary.org/b/id/863434-L.jpg",
        "rating": 3,
        "numberrating": 165,
        "id": 34
    }
];
//# sourceMappingURL=index.js.map