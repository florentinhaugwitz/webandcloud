function displayCountRatings(catData){
    const labels = catData.map(row => row.rating)
    
      const data = {
        labels: labels,
        datasets: [{
          label: 'Number of Books per rating',
          data: catData.map(row => row.number_books),
          backgroundColor: "red",
          hoverOffset: 4
        }],
        colorRangeInfo: {
            colorStart: 0,
            colorEnd: 1,
            useEndAsStart: false,
            },
        
      };
      const config = {
        type: 'bar',
        data: data,
        options: {}
      };
    const myChart = new Chart(
        document.getElementById('countChart'),
        config
      )
}

function displayAverageCategories(catData){
  const labels = catData.map(row => row.category)
  
    const data = {
      labels: labels,
      datasets: [{
        label: 'Average Rating per category',
        data: catData.map(row => row.number_books),
        backgroundColor: "blue",
        hoverOffset: 4
      }],
      colorRangeInfo: {
          colorStart: 0,
          colorEnd: 1,
          useEndAsStart: false,
          },
      
    };
    const config = {
      type: 'bar',
      data: data,
      options: {}
    };
  const myChart = new Chart(
      document.getElementById('averageChart'),
      config
    )
}

function getCountPerRatingChart() {
  fetch('/api/rating-count')
  .then(data => data.json())
  .then(catData => { displayCountRatings(catData) })
}

function getAveragePerCategoryChart(search){
  fetch('/api/category-average')
  .then(data => data.json())
  .then(catData => { displayAverageCategories(catData) })
}

function installOtherEventHandlers() {
  document.getElementById("dropbtn").onclick = dropDownFunction
  window.onclick = closeMenu
}

window.onload = () => {
  getCountPerRatingChart() // If no parameter is given, search is undefined
  getAveragePerCategoryChart()
  installOtherEventHandlers()
}