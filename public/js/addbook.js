function get_new_book_data() {
  const authorsinputslist = document.getElementsByClassName("bookauthor")
  const authorsInput = []
  for (let id = 0; id < authorsinputslist.length; id++) {
    authorsInput.push(authorsinputslist[id].value)
  }

  const titleInput = document.getElementById("booktitle").value
  const ratingInput = parseInt(document.getElementById("rating").value)
  const categoryInput = document.getElementById("category").value
  const imageInput = document.getElementById("url").value

  const dict = {
    title: titleInput,
    authors: authorsInput,
    rating: ratingInput,
    category: categoryInput,
    image: imageInput,
    numberrating: 1
  }
  addNewBook(dict)
}

function add_further_author() {

  const text = document.createElement("input")
  text.className = "inputAddbook bookauthor"
  text.name = "bookauthor"
  text.type = "text"
  text.value = document.getElementById("bookauthor").value
  document.getElementById("bookauthor").value = ""

  const label = document.createElement("label")
  label.htmlFor  = "bookauthor"
  label.textContent = "Further author:"

  const div = document.createElement("DIV")
  div.className = "newBookInputValue"
  
  div.appendChild(label)
  div.appendChild(text)


  document.getElementById("authorBooksDiv").after(div);
}

function addNewBook(bookObject) {

  fetch("/api/books", {
      method: "POST",
      headers: {
          'content-type':'application/json;charset=utf-8'
      },
      body: JSON.stringify(bookObject)
  })
  window.location.replace("/");
}

function installOtherEventHandlers() {
  addAuthorButton = document.getElementById("addAuthor")
  addAuthorButton.onclick = add_further_author

  submitNewBookButton = document.getElementById("submitNewBook")
  submitNewBookButton.onclick = get_new_book_data

  document.getElementById("dropbtn").onclick = dropDownFunction
  window.onclick = closeMenu
}

window.onload = () => {
  installOtherEventHandlers()
}



