function createBookBox(book) {

    // Creation of image element
    const img = document.createElement('IMG')
    img.src = book.image
    img.width="200"
    img.height="300"
    img.alt = book.title
   
    // Creation of title element
    const h2 = document.createElement("H2")
    h2.textContent = book.title
    
    // Creation of author element
    const h3Authors = document.createElement("H3")
    h3Authors.textContent = "By " + book.authors.join(" & ")

    const h3Category = document.createElement("H3")
    h3Category.className = "category"
    h3Category.textContent = "Category: " + book.category

    // Appending title and author to div element
    titleDiv = document.createElement("DIV")
    titleDiv.className = "titleBox"
    titleDiv.append(h2, h3Authors, h3Category)

    // Creation of star div
    starDiv = document.createElement("DIV")
    // For loop for all span elements
    for (let i = 1; i < 7; i++) {
        const span = document.createElement("SPAN")
        if (i <= book.rating) {
            span.className =  "star yellowstar"
            span.textContent = "★"
        }
        else if (i === 6){
            span.textContent = "(" + book.numberrating + ")"
        }
        else {
            span.className =  "star"
            span.textContent = "★"
        }
        starDiv.append(span)
    }
    
    // Creation of outer div
    const outerDiv = document.createElement("DIV")
    outerDiv.className = "bookbox"
    outerDiv.append(img, titleDiv, starDiv)

    // Creation of list element
    const li = document.createElement("LI")
    li.id = "#" + book.id
    li.append(outerDiv)

    return li
}

function fillBooks(books) {
    const list = document.getElementById("listofbooks")
    list.innerHTML = ""
    for (const idx in books) {
        const li = createBookBox(books[idx])
        list.append(li)
    }
}

function loadAndFillBooks(search) {
    let query = ""
    if( search != undefined )
        query = `?search=${search}`

    fetch('/api/books'+query)
    .then(data => data.json())
    .then(books => { fillBooks(books) })
}

function applySearch() {
    const input = document.getElementById("searchbox").value
    loadAndFillBooks(input)
}

function loadandFillCategoryBooks(search){
    let query = ""
    if( search.target.value != undefined )
        query = `?search=${search.target.value}`

    fetch('/api/category'+query)
    .then(data => data.json())
    .then(books => { fillBooks(books) })
}

function installOtherEventHandlers() {
    document.getElementById("searchbutton").onclick = applySearch

    document.getElementById("dropbtn").onclick = dropDownFunction
    window.onclick = closeMenu
}

function createCategory(category){
    a = document.createElement("a")
    a.href = "#"
    a.className = "category-tag"
    a.value = category.category
    a.onclick = loadandFillCategoryBooks
    a.textContent = category.category + " (" + category.number_books + ")"

    p = document.createElement("p")
    p.appendChild(a)
    return p
}

function addCategories(categories) {
    const h3 = document.getElementById("categoryH3")
    for (const idx in categories) {
        const p = createCategory(categories[idx])
        h3.after(p)
    }
}

function loadCatgories(){
    fetch('/api/category-count')
    .then(data => data.json())
    .then(category => { addCategories(category) })
}

window.onload = () => {
    loadAndFillBooks() // If no parameter is given, search is undefined
    loadCatgories()

    installOtherEventHandlers()
}


