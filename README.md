# Library - Adding and selecting your favorite books

ExpressJS application in typescript to view a sample library with dynamic adjustments and statistics

## Installation
To start the project please create the same environment with the use of the adpro.yml file
```cmd
npm run start
```


## Authors
- Tara Tumbraegel (48333@novasbe.pt)
- Florentin von Haugwitz (48174@novasbe.pt)
