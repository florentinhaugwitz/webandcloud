export interface CategoryData {
    category: string
    number_books: number
}

export interface RatingData {
    rating: number
    number_books: number
}