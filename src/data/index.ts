import { db } from './init'
import { Book } from '../books'
import { CategoryData, RatingData } from '../statistics'

export function getAllBooks(search: string, fn: (books: Book[]) => void) {
  const sql = `
              SELECT b.*, json_group_array(a.name) AS authors
              FROM Book AS b
              INNER JOIN author_book AS ab ON b.id = ab.book_id
              INNER JOIN author AS a ON ab.author_id = a.id
              WHERE b.title LIKE '%' || ? || '%' OR a.name LIKE '%' || ? || '%'
              GROUP BY b.id
              `
  const params: string[] = [search, search]
  return db.all(sql, params, (err, rows) => {
    if (err) {
      console.log("error in database: " + err)
      fn([])
    } else {
      rows.forEach((element, index, array) => {
        array[index].authors = JSON.parse(element.authors)
      })
      fn(rows)
    }
  })
}

export function getBooksByCategory(search: string, fn: (books: Book[]) => void) {
  const sql = `
              SELECT b.*, json_group_array(a.name) AS authors
              FROM Book AS b
              INNER JOIN author_book AS ab ON b.id = ab.book_id
              INNER JOIN author AS a ON ab.author_id = a.id
              WHERE b.category = (?)
              GROUP BY b.id
              `
  const params: string[] = [search]
  return db.all(sql, params, (err, rows) => {
    if (err) {
      console.log("error in database: " + err)
      fn([])
    } else {
      rows.forEach((element, index, array) => {
        array[index].authors = JSON.parse(element.authors)
      })
      fn(rows)
    }
  })
}

export function getOneBook(id: number, fn: (book: Book | null) => void) {
  const sql = `
              SELECT b.*, json_group_array(a.name) AS authors
              FROM Book AS b
              INNER JOIN author_book AS ab ON b.id = ab.book_id
              INNER JOIN author AS a ON ab.author_id = a.id
              WHERE b.id = ?
              GROUP BY b.id
              `

  const params: string[] = ["" + id]
  return db.get(sql, params, (err, row) => {
    if (err) {
      console.log("error in database: " + err)
      fn(null)
    } else {
      row.authors = JSON.parse(row.authors)
      fn(row)
    }
  })
}

export function addOneBook(s: Book) {

  db.serialize(() => {
      const insertBook =
      `
      INSERT INTO book (id,title, image, rating, numberrating, category) VALUES (?,?,?,?,?,?)
      `
      db.run(insertBook, [s.id, s.title, s.image, s.rating, s.numberrating, s.category])

      const insertAuthor =
      `
      INSERT OR IGNORE INTO author (name) VALUES (?)
      `
      s.authors.forEach(author => {
        db.run(insertAuthor, [author])
      })

      const insertAuthorBook =
      `
      INSERT INTO author_book (author_id, book_id) VALUES (?,?)
      `

      const selAuthor =
      `
      SELECT id FROM author WHERE name = (?)
      `

      const selBook =
      `
      SELECT MAX(id) AS max_id FROM book
      `
      s.authors.forEach(author => {
        db.get(selAuthor, author, (errSelAuthor, authorId) => {
          if (errSelAuthor) {
            console.log("Error in database: " + errSelAuthor)
          } else {
            db.get(selBook, (errSelBook, bookId) => {
              if (errSelBook) {
                console.log("Error in database: " + errSelBook)
              } else{
                db.run(insertAuthorBook, [authorId.id, bookId.max_id])
              }
            })
          }
        })
      })
    })
  }

export function getCountRatingData(fn: (ratingData: RatingData[]) => void) {
  const sql = `
              SELECT rating, count(id) AS number_books
              FROM Book
              GROUP BY rating
              `
  return db.all(sql, (err, rows) => {
    if (err) {
      console.log("error in database: " + err)
      fn([])
    } else {
      fn(rows)
    }
  })
}

export function getCountCategoryData(fn: (categoryData: CategoryData[]) => void) {
  const sql = `
              SELECT category, count(id) AS number_books
              FROM Book
              GROUP BY category
              `
  return db.all(sql, (err, rows) => {
    if (err) {
      console.log("error in database: " + err)
      fn([])
    } else {
      fn(rows)
    }
  })
}


export function getAverageCategoryData(fn: (categoryData: CategoryData[]) => void) {
  const sql = `
              SELECT category, avg(rating) AS number_books
              FROM Book
              GROUP BY category
              `
  return db.all(sql, (err, rows) => {
    if (err) {
      console.log("error in database: " + err)
      fn([])
    } else {
      fn(rows)
    }
  })
}