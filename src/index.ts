import express from 'express'
import './data'
import { addOneBook, getAllBooks, getOneBook, getCountRatingData, getCountCategoryData, getAverageCategoryData, getBooksByCategory } from './data'

const app = express()
const port = 8080

app.use(express.static('public'))

// Getting all books, with search
app.get('/api/books', (req,res) => {
    const search:string = ( req.query.search || "" ) as string
    getAllBooks(search, (data) => { res.send(JSON.stringify(data)) })
})

// Getting books by category
app.get('/api/category', (req,res) => {
    const search:string = ( req.query.search || "" ) as string
    getBooksByCategory(search, (data) => { res.send(JSON.stringify(data)) })
})

// Getting one book
app.get('/api/books/:id', (req,res) => {
    const bookId = parseInt(req.params.id,10)
    getOneBook(bookId, (book) => {
        if (book != null) res.send(JSON.stringify(book))
        else {
            res.status(404)
            res.send()
        }
    })
})

// Adding one book
app.post('/api/books', (req,res) => {
    let body = ""
    req
    .on('data', (data) => body += data)
    .on('end', () => { addOneBook(JSON.parse(body)) })
})

// Getting category numbers
app.get('/api/category-count', (req,res) => {
    getCountCategoryData((data) => { res.send(JSON.stringify(data)) })
})

// Getting rating numbers
app.get('/api/rating-count', (req,res) => {
    getCountRatingData((data) => { res.send(JSON.stringify(data)) })
})

// Getting category average
app.get('/api/category-average', (req,res) => {
    getAverageCategoryData((data) => { res.send(JSON.stringify(data)) })
})

app.listen( port, () => {
    console.log( `server started at http://localhost:${ port }` );
} );





























